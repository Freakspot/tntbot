var VOTES = {
    "red": 0,
    "blue": 0,
    "green": 0,
    "yellow": 0
};

var ctx = document.getElementById("chart");
var options = {
    animation: false,
    cutoutPercentage: 35
};

var data = {
    labels: ["Red", "Blue", "Green", "Yellow"],
    datasets: [{
        data: [VOTES["red"], VOTES["blue"], VOTES["green"], VOTES["yellow"]],
        backgroundColor: [
            "#8b1414",
            "#0055ff",
            "#1e872a",
            "#a99200"
        ]
    }]
};


// CREATE INITIAL CHART
votesChart = new Chart(ctx,{
    type: 'doughnut',
    data: data,
    options: options
});


// UPDATE CHART WITH NEW DATA
setInterval(function loadXMLDoc() {
    
    // GET NEW DATA
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            VOTES = JSON.parse(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", "/votes", true);
    xmlhttp.send();

    // INSERT NEW DATA
    for (i = 0; i < votesChart.data.datasets[0].data.length; i++) {
        votesChart.data.datasets[0].data[i] = VOTES[Object.keys(VOTES)[i]];
    }

    votesChart.update();

}, 1000);