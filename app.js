var tmi = require('tmi.js');
var express = require('express');
var app = express();


// GET VARIABLES FROM CONFIG FILE
var CHANNELNAME = process.env.CHANNELNAME;
var PASSWORD    = process.env.PASSWORD;
var USERNAME    = process.env.USERNAME;


// OMG CORS PLS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// EXPRESS WEIRDNESS
app.use(express.static('public'));


// API
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/chart.html');
});

app.get('/votes', function (req, res) {
    res.json(votes);
});


// START THE WEBSERVER
var port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log('Started webserver on port ' + port);
});


// START THE TWITCHBOT
var options = {
    options: {
        debug: true
    },
    identity: {
        username: USERNAME,
        password: PASSWORD
    },
    channels: [CHANNELNAME]
};

var client = new tmi.client(options);
client.connect();


// DO STUFF
client.on("chat", function (channel, user, message, self) {

    if (message.indexOf("!") == 0) {

        // MOD COMMANDS FOR VOTING SYSTEM

        // !PING
        // Pong.
        if (message.indexOf("!ping") == 0) {
            if (user.mod) {
                client.say(CHANNELNAME, "Pong.");
            }
        }

        // ENABLE A SINGLE COLOR
        // !enable <color>
        else if (message.indexOf("!enable") == 0) {
            if (user.mod) {
                var color = message.substring(8, message.length);
                if (isValidColor(color)) {
                    enabled[color] = true;
                }
            }
        }

        // DISABLE A SINGLE COLOR
        // !disable <color>
        else if (message.indexOf("!disable") == 0) {
            if (user.mod) {
                var color = message.substring(9, message.length);
                if (isValidColor(color)) {
                    enabled[color] = false;
                }
            }
        }

        // ENABLE VOTING OF MULTIPLE COLORS AT ONCE
        // !fight <color> <color>
        else if (message.indexOf("!fight") == 0) {
            if (user.mod) {
                var split = message.split(" ");
                for (i = 1; i < split.length; i++) {
                    if (isValidColor(split[i])) {
                        enabled[split[i]] = true;
                    }
                }
            }
        }

        // RESET ALL VOTES AND DISABLE ALL COLORS
        // !reset
        else if (message === "!reset") {
            if (user.mod) {
                enabled = {
                    'red': false,
                    'blue': false,
                    'green': false,
                    'yellow': false,
                };

                votes = {
                    "red": 0,
                    "blue": 0,
                    "green": 0,
                    "yellow": 0
                };

                hasVoted = {};
            }
        }

        // WIPE ALL VOTES BUT KEEP ALL COLORS
        // !wipe
        else if (message === "!wipe") {
            if (user.mod) {

                votes = {
                    "red": 0,
                    "blue": 0,
                    "green": 0,
                    "yellow": 0
                };

                hasVoted = {};
            }
        }

        // PLEB COMMANDS FOR VOTING SYSTEM
        else if (message === "!vote red" || message === "!vote blue" || message === "!vote green" || message === "!vote yellow") {
            Vote(channel, user, message, self);
        }
        
        // IF SHIT THEN
        else {
            console.log("Invalid command or not sufficient permissions.");
        }
	}
});

var hasVoted = {};
var enabled = {
    'red': false,
    'blue': false,
    'green': false,
    'yellow': false,
};
var votes = {
    "red": 0,
    "blue": 0,
    "green": 0,
    "yellow": 0
};


// HELPER FUNCTIONS
function isValidColor(color) {
    if (color === "red" || color === "blue" || color === "green" || color === "yellow" ) {
        return true;
    }
};

function Vote(channel, user, message, self) {
    var color = message.substring(6, message.length);
    
    if (enabled[color]) {
        if (user["user-id"] in hasVoted) {
            votes[color] += 1;
            votes[hasVoted[user["user-id"]]] -= 1;
            hasVoted[user["user-id"]] = color;

            console.log(votes);
        } else {
            votes[color] += 1;
            hasVoted[user["user-id"]] = color;

            console.log(votes);
        }
    } else {
        if (self) return;
        client.whisper(user.username, color + " doesn't play in this match!");
    }
};